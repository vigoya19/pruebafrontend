# Prueba Técnica CIDENET (API)

_Gestión de empleados_

## Comenzando 🚀

_Para obtener una copia del proyecto en su máquina local use Git clone_

```
git clone https://gitlab.com/vigoya19/frontendcidenet.git
```

### Pre-requisitos 📋

_Nodejs 12.18.3 o más actual_

[Angular CLI](https://github.com/angular/angular-cli) version 10.0.6 o superior.

### Instalación 🔧

_En la raiz del proyecto, en la consola instalaremos las dependencias_

```
npm install
```

## Servidor de desarrollo

Corremos `ng serve` Para un servidor de desarrollo. Navegamos a `http://localhost:4200/`.
