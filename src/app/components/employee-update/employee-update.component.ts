import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EmployeesService } from '../../services/employees.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-employee-update',
  templateUrl: './employee-update.component.html',
  styleUrls: ['./employee-update.component.css']
})
export class EmployeeUpdateComponent implements OnInit {
  employee = [];
  id: number;

  employeeFomr = new FormGroup({
    primerApellido: new FormControl('', Validators.required),
    segundoApellido: new FormControl('', Validators.required),
    primerNombre: new FormControl('', Validators.required),
    otrosNombres: new FormControl('', Validators.required),
    paisEmpleo: new FormControl('', Validators.required),
    tipoIdentificacion: new FormControl('', Validators.required),
    numeroIdentificacion: new FormControl('', Validators.required),
    fechaIngreso: new FormControl('', Validators.required),
    area: new FormControl('', Validators.required)
  });

  constructor(
    private _employeeService: EmployeesService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) {}

  ngOnInit(): void {
    this._activatedRoute.params.subscribe(params => {
      this.id = params['id'];

      this._employeeService.getEmployee(this.id).subscribe(employee => {
        this.employee = employee['data'][0];
        console.log(this.employee, 'ESTO QUE EE?');
        this.setValues();
      });
    });
  }

  setValues() {
    this.employeeFomr
      .get('primerApellido')
      .setValue(this.employee['primerApellido']);
    this.employeeFomr
      .get('segundoApellido')
      .setValue(this.employee['segundoApellido']);
    this.employeeFomr
      .get('primerNombre')
      .setValue(this.employee['primerNombre']);
    this.employeeFomr
      .get('otrosNombres')
      .setValue(this.employee['otrosNombres']);
    this.employeeFomr.get('paisEmpleo').setValue(this.employee['paisEmpleo']);

    this.employeeFomr
      .get('tipoIdentificacion')
      .setValue(this.employee['tipoIdentificacion']);

    this.employeeFomr
      .get('numeroIdentificacion')
      .setValue(this.employee['numeroIdentificacion']);

    this.employeeFomr.get('area').setValue(this.employee['area']);

    const fecha = new Date(this.employee['fechaIngreso']);
    this.employeeFomr
      .get('fechaIngreso')
      .setValue(fecha.toISOString().slice(0, 10));
  }

  onSubmit() {
    const {
      primerApellido,
      segundoApellido,
      primerNombre,
      otrosNombres,
      paisEmpleo,
      tipoIdentificacion,
      numeroIdentificacion,
      fechaIngreso,
      area
    } = this.employeeFomr.value;

    const newEmployee = {
      primerApellido: primerApellido,
      segundoApellido: segundoApellido,
      primerNombre: primerNombre,
      otrosNombres: otrosNombres,
      paisEmpleo: paisEmpleo,
      tipoIdentificacion: tipoIdentificacion,
      numeroIdentificacion: numeroIdentificacion,
      fechaIngreso: fechaIngreso,
      area: area
    };

    this._employeeService
      .updateEmployee(this.id, newEmployee)
      .subscribe((data: any) => {
        if (data.code) {
          alert('Empleado actualizado con éxito!');
          this.employeeFomr.reset();
          this._router.navigate(['/employees']);
        } else {
          alert(data.error);
        }
      });
  }
}
