import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeesService } from '../../services/employees.service';

@Component({
  selector: 'app-busqueda-employees',
  templateUrl: './busqueda-employees.component.html',
  styleUrls: ['./busqueda-employees.component.css']
})
export class BusquedaEmployeesComponent implements OnInit {
  filteredEmployees = [];
  allEmployees = [];
  numeroIdentificacion = '';
  newEmployees = [];
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _employeeService: EmployeesService
  ) {}

  ngOnInit(): void {
    this._activatedRoute.params.subscribe(params => {
      console.log(params);
      this.numeroIdentificacion = params['numeroIdentificacion'];
      this.showEmployees(this.numeroIdentificacion);
    });
  }

  showEmployees(numeroIdentificacion) {
    this.newEmployees = [];
    this.filteredEmployees = [];
    this._employeeService.getEmployees().subscribe(employees => {
      this.allEmployees = employees['data'];
      this.allEmployees.forEach(employee => {
        if (
          employee.numeroIdentificacion
            .toLowerCase()
            .includes(numeroIdentificacion.toLowerCase()) ||
          employee.primerNombre
            .toLowerCase()
            .includes(numeroIdentificacion.toLowerCase()) ||
          employee.primerNombre
            .toLowerCase()
            .includes(numeroIdentificacion.toLowerCase()) ||
          employee.primerApellido
            .toLowerCase()
            .includes(numeroIdentificacion.toLowerCase()) ||
          employee.segundoApellido
            .toLowerCase()
            .includes(numeroIdentificacion.toLowerCase()) ||
          employee.segundoApellido
            .toLowerCase()
            .includes(numeroIdentificacion.toLowerCase()) ||
          employee.paisEmpleo
            .toLowerCase()
            .includes(numeroIdentificacion.toLowerCase()) ||
          employee.correo
            .toLowerCase()
            .includes(numeroIdentificacion.toLowerCase()) ||
          employee.area
            .toLowerCase()
            .includes(numeroIdentificacion.toLowerCase())
        ) {
          this.filteredEmployees.push(employee);
          console.log('si se cumple');
          console.log(this.filteredEmployees);
        } else {
          console.log('no se cumple');
        }
      });
    });
  }
}
