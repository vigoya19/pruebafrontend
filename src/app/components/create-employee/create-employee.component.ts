import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeesService } from '../../services/employees.service';
@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
  regAccents = /^[A-Z]+(\s*[A-Z]*)*[A-Z]+$/;
  numeroIdentificacionRegex = /^([a-zA-Z0-9_-]){1,20}$/;
  employeeFomr = new FormGroup({
    primerApellido: new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(20),
      Validators.pattern(this.regAccents)
    ]),
    segundoApellido: new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(20)
    ]),
    primerNombre: new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(20)
    ]),
    otrosNombres: new FormControl('', [
      Validators.required,
      Validators.minLength(1),
      Validators.maxLength(20)
    ]),
    paisEmpleo: new FormControl('Colombia', Validators.required),
    tipoIdentificacion: new FormControl(
      'Cedula de Ciudadania',
      Validators.required
    ),
    numeroIdentificacion: new FormControl('', [
      Validators.required,
      Validators.pattern(this.numeroIdentificacionRegex)
    ]),
    fechaIngreso: new FormControl('', Validators.required),
    area: new FormControl('Administracion', Validators.required)
  });

  constructor(
    private _employeeService: EmployeesService,
    private _router: Router
  ) {}

  ngOnInit(): void {}

  setValues() {
    this.employeeFomr.controls['country'].setValue('Colombia', {
      onlySelf: true
    });
  }

  onSubmit() {
    const {
      primerApellido,
      segundoApellido,
      primerNombre,
      otrosNombres,
      paisEmpleo,
      tipoIdentificacion,
      numeroIdentificacion,
      fechaIngreso,
      area
    } = this.employeeFomr.value;

    const newEmployee = {
      primerApellido: primerApellido,
      segundoApellido: segundoApellido,
      primerNombre: primerNombre,
      otrosNombres: otrosNombres,
      paisEmpleo: paisEmpleo,
      tipoIdentificacion: tipoIdentificacion,
      numeroIdentificacion: numeroIdentificacion,
      fechaIngreso: fechaIngreso,
      area: area
    };

    this._employeeService.createEmployee(newEmployee).subscribe((data: any) => {
      if (data.code) {
        alert('Empleado creado con éxito!');
        this.employeeFomr.reset();
        this._router.navigate(['/employees']);
      } else {
        console.log(data);
      }
    });
  }

  onSubmitFirst() {}
}
