import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeesService } from 'src/app/services/employees.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {
  @Input() employee: any = {};

  constructor(
    private _employeeService: EmployeesService,
    private _router: Router
  ) {}

  delete(): void {
    if (
      confirm(
        `Are you sure to delete ${this.employee.numeroIdentificacion} - ${this.employee.primerNombre}`
      )
    ) {
      this._employeeService.deleteEmployee(this.employee.id).subscribe(data => {
        console.log(data);
        alert('Empleado eliminado con éxito!');
        delete this.employee;
        this._router.navigate(['/employees']);
        this._employeeService.getEmployees();
      });
    }
  }

  ngOnInit(): void {
    console.log(this.employee);
  }
}
