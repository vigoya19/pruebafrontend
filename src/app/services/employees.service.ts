import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  baseUrl: string = 'http://localhost:3000/';

  constructor(private http: HttpClient) {}

  getEmployees() {
    return this.http.get(this.baseUrl + 'employee');
  }

  getEmployee(id) {
    return this.http.get(this.baseUrl + 'employee/' + id);
  }

  createEmployee(employee) {
    return this.http.post(`${this.baseUrl}employee`, employee);
  }

  updateEmployee(id, employee) {
    return this.http.put(`${this.baseUrl}employee/${id}`, employee);
  }

  deleteEmployee(id) {
    return this.http.delete(`${this.baseUrl}employee/${id}`);
  }
}
