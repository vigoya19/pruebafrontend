import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {
  transform(employees: any[], page: number = 0): any[] {
    return employees.slice(page, page + 10);
  }
}
